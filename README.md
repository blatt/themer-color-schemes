# Themer Color Schemes [![pipeline status](https://gitlab.com/blatt/themer-color-schemes/badges/master/pipeline.svg)](https://gitlab.com/blatt/themer-color-schemes/commits/build)

My custom color scheme(s), built with **themer**.

## Installation

Download or clone this repository if you want to edit the themes and want to build them yourself.

## How to use?

Run `npm run build` for running the build script and create all custom themes in the folder `build`.

## Color Schemes

At the moment, the script uses only the base16-gruvbox-scheme.

### Themes generated

* Alfred 3
* Chrome
* iTerm 2
* SublimeText 3
* VIM
* VIM Lightline
* Visual Studio Code
* xCode

### Wallpaper generated
The themer config generates the octagon Wallpaper for
* Apple Watch
* iPhone x
* iPad Air
* MacBook Pro

## Credits
* [mjswensen/themer: 🎨 themer takes a set of colors and generates themes for your apps (editors, terminals, wallpapers, and more).](https://github.com/mjswensen/themer) © MIT
* [dawikur/base16-gruvbox-scheme: Retro groove color scheme for the base16](https://github.com/dawikur/base16-gruvbox-scheme) © MIT

## License

© Distributed under the terms of the [MIT License](https://gitlab.com/blatt/themer-color-schemes/raw/master/LICENSE).
